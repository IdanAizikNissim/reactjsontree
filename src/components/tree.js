import React from 'react';
import Leaf from './leaves/leaf';

class Tree extends React.Component {
    render() {
        let elems = [];
        for (let field in this.props.data) {
            let val = this.props.data[field];
            switch(typeof(val)) {
                case 'object':
                    elems.push(
                        <Leaf name={field} value={null} deep={this.props.deep} >
                            <Tree data={val} deep={(this.props.deep + 1)} />
                        </Leaf>
                    );
                break;
                default:
                    elems.push(
                        <Leaf name={field} value={val} deep={this.props.deep} />
                    );
            }
        }

        return (
            <ul>
                {elems}
            </ul>
        );
    }
}

export default Tree;