import React from 'react';
import {LeafVideo, LeafImage, LeafString, LeafNumber} from './leafTypes';
import {getUrlType, UrlType, lightenDarkenColor} from '../helpers';

class Leaf extends React.Component {
    renderCorrespondingStringCompo(value) {
        switch (getUrlType(value)) {
            case UrlType.Video:
            return <LeafVideo value={value} />
            case UrlType.Image:
            return <LeafImage value={value} />
            case UrlType.Unknown:
            return <LeafString value={value} />
        }
    }

    render() {
        let children = this.props.children;
        if (!children) {
            switch (typeof(this.props.value)) {
                case 'string':
                children = this.renderCorrespondingStringCompo(this.props.value);
                break;
                case 'number':
                children = <LeafNumber value={this.props.value} />
                break;
            }
        }
        
        return (
            <li style={{color: lightenDarkenColor('#0D76DF', 20 * this.props.deep)}}>
                {this.props.name}
                {children}
            </li>
        )
    }
}

export default Leaf;