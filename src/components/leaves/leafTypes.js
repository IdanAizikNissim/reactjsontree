import React from 'react';

class LeafValue extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.value,
        };
    }

    view() {
        return (
            this.state.value
        );
    }

    render() {
        return this.view();
    }
}

export class LeafVideo extends LeafValue { 
    view() {
        return (
            <video width="320" height="240" controls>
                <source src={this.props.value} type="video/mp4"/>
            </video>
        );
    }
}

export class LeafImage extends LeafValue {   
    view() {
        return (
            <img src={this.props.value}/>
        );
    } 
}

export class LeafString extends LeafValue { 
    view() {
        return (
            <input value={this.props.value}/>
        );
    } 
}

export class LeafNumber extends LeafValue {
    view() {
        return (
            <input type="number" value={this.props.value}/>
        );
    }   
}