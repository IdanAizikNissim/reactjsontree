import React from 'react';
import ReactDOM from 'react-dom';
import Tree from './components/tree';
import data from './data.json';

ReactDOM.render(
    <Tree data={data} deep={1} />,
    document.getElementById('root')
);